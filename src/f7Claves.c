#include "hashtable.h"
char **claves(hashtable *tabla, int *conteo){
	char** claves=(char**)malloc(sizeof(char*)*(numeroElementos(tabla)));
	objeto *elemento1;
	int contador=0;
	for(int i =0;i<(tabla->numeroBuckets);i++){
		elemento1 = tabla->buckets[i];
		if(elemento1!=NULL){
			claves[contador]=elemento1->clave;
			objeto *elemento2=elemento1->siguiente;
			contador++;
			
			while(elemento2!=NULL){
				
				claves[contador]=elemento2->clave;
				elemento2 = elemento2->siguiente;
				contador++;
			}
		}
	}
	
	*conteo=contador;
	return claves;
}
