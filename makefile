all:	bin/estatico bin/dinamico

bin/estatico:	obj/prueba.o include/libhashtab.a
		gcc -Wall $^ -static -o $@ 

bin/dinamico:	obj/prueba.o include/libhashtab.so
		gcc  -Iinclude/ -Wall $^ -o $@ -L./include

obj/prueba.o:	src/prueba.c
		gcc -Iinclude/ -Wall $< -c -o $@

include/libhashtab.so:	obj/f1Crear.o obj/f2Elementos.o obj/f3Insertar.o obj/f4Obtener.o obj/f5Remover.o  obj/f6Borrar.o obj/f7Claves.o obj/f8Contiene.o obj/f9Valores.o obj/f10Hash.o
		gcc -shared -o $@ $^
include/libhashtab.a:	obj/f1Crear.o obj/f2Elementos.o obj/f3Insertar.o obj/f4Obtener.o obj/f5Remover.o  obj/f6Borrar.o obj/f7Claves.o obj/f8Contiene.o obj/f9Valores.o obj/f10Hash.o
		ar rcs $@ $^

obj/f1Crear.o: src/f1Crear.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f2Elementos.o: src/f2Elementos.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f3Insertar.o: src/f3Insertar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f4Obtener.o: src/f4Obtener.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f5Remover.o: src/f5Remover.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f6Borrar.o: src/f6Borrar.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f7Claves.o: src/f7Claves.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f8Contiene.o: src/f8Contiene.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f9Valores.o: src/f9Valores.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@
obj/f10Hash.o: src/f10Hash.c
	gcc -Wall -Iinclude -fPIC $< -c -o $@


clean:                  
	rm obj/*.o bin/* lib/*
